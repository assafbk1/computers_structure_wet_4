/* 046267 Computer Architecture - Winter 2019/20 - HW #4 */

#include "core_api.h"
#include "sim_api.h"

#include <stdio.h>
#include <string.h>

#define NUM_OF_REGS 8



class thread{

public:

    int tid;
    int pc;
    int wait_cycles;
    bool is_halt;

    tcontext context;

    thread(int tid): tid(tid), pc(0), wait_cycles(0), is_halt(false) {
        for (int i=0; i<NUM_OF_REGS; i++) {
            context.reg[i]=0;
        }
    }

};

/** global variables **/

thread** blocked_threads_array;
thread** finegrained_threads_array;
int finegrained_cycles;
int finegrained_instructions;
int blocked_cycles;
int blocked_instructions;

/** helper functions: **/

/*
// returns the min waiting time of all threads.
int find_min_waiting(){

    int min = -1;

    for (int i = 0; i < SIM_GetThreadsNum(); i++){

        if(threads_array[i]->is_halt) continue;

        if(threads_array[i]->wait_cycles < min or min < 0 ){ // min < 0 is for initialization of min
            min = threads_array[i]->wait_cycles;
        }

    }

    return min;
}
*/

// decrease the waiting time of all threads by dec_val (the min wait time is 0)
void dec_waiting(int dec_val, bool is_finegrained){

    thread** threads_array;
    if(is_finegrained){
        threads_array = finegrained_threads_array;
    } else{
        threads_array = blocked_threads_array;
    }
    while(dec_val) {
        for (int i = 0; i < SIM_GetThreadsNum(); i++) {
            if(threads_array[i]->wait_cycles > 0){
                threads_array[i]->wait_cycles--;
            }
        }
        dec_val--;
    }
}
// function returns true if our current cycle is an idle one.
bool is_cur_cycle_idle(bool is_finegrained){

    thread** threads_array;
    if(is_finegrained){
        threads_array = finegrained_threads_array;
    } else{
        threads_array = blocked_threads_array;
    }

    for(int i=0; i < SIM_GetThreadsNum(); i++){
        if(threads_array[i]->wait_cycles == 0 and !(threads_array[i]->is_halt)){
            // if there's an active thread we return false
            return false;
        }
    }
    // otherwise - return true - we are idle for now
    return true;

}

// BLOCKED MT: function returns the thread_id of the thread that should run on this cycle - RR method
// returns -1 in case of error (no "ready-to-run" thread found)
int get_thread_to_run_blocked(int cur_tid){

    int tid = cur_tid;
    int num_of_threads = SIM_GetThreadsNum();
    while(tid != (cur_tid + num_of_threads)){ // the loop ends when we reach our thread again
        if(!(blocked_threads_array[tid % num_of_threads]->is_halt) and blocked_threads_array[tid % num_of_threads]->wait_cycles == 0){
            return (tid % num_of_threads); // we return the first "ready-to-run" thread
        }
        tid++;
    }
    return -1;
}

// FINE-GRAINED MT: function returns the thread_id of the thread that should run on this cycle - RR method
// returns -1 in case of error (no "ready-to-run" thread found)
int get_thread_to_run_finegrained(int cur_tid){

    int num_of_threads = SIM_GetThreadsNum();
    int tid = (cur_tid+1)%num_of_threads;
    while(tid != (cur_tid + 1 + num_of_threads)){ // the loop ends when we reach our starting thread again (cur+1)
        if(!(finegrained_threads_array[tid % num_of_threads]->is_halt) and finegrained_threads_array[tid % num_of_threads]->wait_cycles == 0){
            return (tid % num_of_threads); // we return the first "ready-to-run" thread
        }
        tid++;
    }
    return -1;
}

/** end of helpers **/


void CORE_BlockedMT() {

    int inst_cnt = 0;
    int cycle_cnt = 0;

    bool cs = false; // context switch
    int cs_cnt = 0;  // counter for number of cycles left to the cs

    int num_of_threads = SIM_GetThreadsNum();
    int num_of_running_threads = num_of_threads;
    thread** threads = (thread**)malloc(sizeof(thread)*num_of_threads);
    blocked_threads_array = threads;

    for (int i=0; i<num_of_threads; i++) {
        threads[i] = new thread(i);
    };

    Instruction inst = {CMD_NOP,0,0,0,false};

    int cur_id = 0;
    int next_tid = -1;
    bool is_execute = false;

    while(num_of_running_threads > 0) {

        if (cs == true) {
            if (cs_cnt>0) {
                cs_cnt--;
            } else {
                cs = false;
                is_execute = true;
            }

        } else {
            if (is_cur_cycle_idle(false) == false) {
                assert(get_thread_to_run_blocked(cur_id) != -1); // if we got '-1' it means we suppose to be idle - ERROR
                next_tid = get_thread_to_run_blocked(cur_id);
                if (next_tid != cur_id) {
                    cs = true;
                    cs_cnt = SIM_GetSwitchCycles()-1; // we decrease one because of our method of modeling the
                    cur_id = next_tid;
                } else {
                    is_execute = true;
                }
            }
        }


        if (is_execute == true) {
            thread* cur_thread = threads[cur_id];
            SIM_MemInstRead(cur_thread->pc, &inst, cur_id);
            cur_thread->pc += 1;
            inst_cnt+=1;

            switch (inst.opcode) {
                case CMD_NOP: // NOP
                    break;

                case CMD_ADDI:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] + inst.src2_index_imm;
                    break;

                case CMD_SUBI:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] - inst.src2_index_imm;
                    break;

                case CMD_ADD:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] + cur_thread->context.reg[inst.src2_index_imm];
                    break;

                case CMD_SUB:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] - cur_thread->context.reg[inst.src2_index_imm];
                    break;

                case CMD_LOAD:
                    if (inst.isSrc2Imm) {
                        SIM_MemDataRead(cur_thread->context.reg[inst.src1_index] + inst.src2_index_imm, cur_thread->context.reg + inst.dst_index);
                    } else {
                        SIM_MemDataRead(cur_thread->context.reg[inst.src1_index] + cur_thread->context.reg[inst.src2_index_imm],  cur_thread->context.reg + inst.dst_index);
                    }

                    cur_thread->wait_cycles = SIM_GetLoadLat() + 1; // we add one because we immediately decrease one at the end of the iteration
                    break;

                case CMD_STORE:
                    if (inst.isSrc2Imm) {
                        SIM_MemDataWrite(cur_thread->context.reg[inst.dst_index] + inst.src2_index_imm, cur_thread->context.reg[inst.src1_index]);
                    } else {
                        SIM_MemDataWrite(cur_thread->context.reg[inst.dst_index] + cur_thread->context.reg[inst.src2_index_imm], cur_thread->context.reg[inst.src1_index]);
                    }

                    cur_thread->wait_cycles = SIM_GetStoreLat() + 1;  // we add one because we immediately decrease one at the end of the iteration
                    break;

                case CMD_HALT:
                    cur_thread->is_halt = true;
                    num_of_running_threads--;
                    break;
            }
        }


        // update data before next iteration
        is_execute = false;
        cycle_cnt++;
        dec_waiting(1,false);

    }

    // when we finish the loop - just update global vars:
    blocked_cycles = cycle_cnt;
    blocked_instructions = inst_cnt;

}

void CORE_FinegrainedMT() {

    int inst_cnt = 0;
    int cycle_cnt = 0;

    int num_of_threads = SIM_GetThreadsNum();
    int num_of_running_threads = num_of_threads;
    thread** threads = (thread**)malloc(sizeof(thread)*num_of_threads);
    finegrained_threads_array = threads;

    for (int i=0; i<num_of_threads; i++) {
        threads[i] = new thread(i);
    };

    Instruction inst = {CMD_NOP,0,0,0,false};

    int cur_id = 0;
    int next_tid = -1;
    bool is_execute = false;

    while(num_of_running_threads > 0) {

        if(cycle_cnt == 0){ // first cycle - we know that we can execute thread 0.
            is_execute = true;
        }

        else if (is_cur_cycle_idle(true) == false) { // not idle
            is_execute = true;
            assert(get_thread_to_run_finegrained(cur_id) != -1); // if we got '-1' it means we suppose to be idle - ERROR
            next_tid = get_thread_to_run_finegrained(cur_id);
            cur_id = next_tid; // no cs penalty so we execute the next thread's instruction in this cycle
        }

        if (is_execute == true) {
            thread* cur_thread = threads[cur_id];
            SIM_MemInstRead(cur_thread->pc, &inst, cur_id);
            cur_thread->pc += 1;
            inst_cnt+=1;

            switch (inst.opcode) {
                case CMD_NOP: // NOP
                    break;

                case CMD_ADDI:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] + inst.src2_index_imm;
                    break;

                case CMD_SUBI:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] - inst.src2_index_imm;
                    break;

                case CMD_ADD:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] + cur_thread->context.reg[inst.src2_index_imm];
                    break;

                case CMD_SUB:
                    cur_thread->context.reg[inst.dst_index] = cur_thread->context.reg[inst.src1_index] - cur_thread->context.reg[inst.src2_index_imm];
                    break;

                case CMD_LOAD:
                    if (inst.isSrc2Imm) {
                        SIM_MemDataRead(cur_thread->context.reg[inst.src1_index] + inst.src2_index_imm, cur_thread->context.reg + inst.dst_index);
                    } else {
                        SIM_MemDataRead(cur_thread->context.reg[inst.src1_index] + cur_thread->context.reg[inst.src2_index_imm],  cur_thread->context.reg + inst.dst_index);
                    }

                    cur_thread->wait_cycles = SIM_GetLoadLat() + 1; // we add one because we immediately decrease one at the end of the iteration
                    break;

                case CMD_STORE:
                    if (inst.isSrc2Imm) {
                        SIM_MemDataWrite(cur_thread->context.reg[inst.dst_index] + inst.src2_index_imm, cur_thread->context.reg[inst.src1_index]);
                    } else {
                        SIM_MemDataWrite(cur_thread->context.reg[inst.dst_index] + cur_thread->context.reg[inst.src2_index_imm], cur_thread->context.reg[inst.src1_index]);
                    }

                    cur_thread->wait_cycles = SIM_GetStoreLat() + 1;  // we add one because we immediately decrease one at the end of the iteration
                    break;

                case CMD_HALT:
                    cur_thread->is_halt = true;
                    num_of_running_threads--;
                    break;
            }
        }


        // update data before next iteration
        is_execute = false;
        cycle_cnt++;
        dec_waiting(1,true);

    }

    // when we finish the loop - just update global vars:
    finegrained_cycles = cycle_cnt;
    finegrained_instructions = inst_cnt;

}

double CORE_BlockedMT_CPI(){
	return (double)(blocked_cycles)/(blocked_instructions);
}

double CORE_FinegrainedMT_CPI(){
    return (double)(finegrained_cycles)/(finegrained_instructions);
}

void CORE_BlockedMT_CTX(tcontext* context, int threadid) {
    for(int i=0;i<NUM_OF_REGS;i++){
        context[threadid].reg[i] = blocked_threads_array[threadid]->context.reg[i];
    }
}

void CORE_FinegrainedMT_CTX(tcontext* context, int threadid) {
    for(int i=0;i<NUM_OF_REGS;i++){
        context[threadid].reg[i] = finegrained_threads_array[threadid]->context.reg[i];
    }
}

